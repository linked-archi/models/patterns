# Linked Archi Patterns


## todo

Creational Patterns:

These patterns deal with the process of object creation. They provide mechanisms for creating objects in a manner suitable to the situation.

Examples: Singleton, Factory Method, Abstract Factory, Builder, Prototype.

Structural Patterns:

These patterns are concerned with the composition of classes and objects. They help to form larger structures with more flexibility.

Examples: Adapter, Bridge, Composite, Decorator, Facade, Proxy.

Behavioral Patterns:

These patterns define the ways in which objects interact and communicate with each other. They focus on the delegation of responsibility between objects.

Examples: Chain of Responsibility, Command, Interpreter, Iterator, Mediator, Memento, Observer, State, Strategy, Template Method, Visitor.

Concurrency Patterns:

These patterns deal with the complexities of concurrent programming, providing solutions for managing multiple threads and their interactions.

Examples: Active Object, Monitor Object, Read-Write Lock, Thread Pool.

Architectural Patterns:

These patterns provide high-level structure for an entire application. They define the organization and flow of control between components or subsystems.

Examples: Model-View-Controller (MVC), Model-View-ViewModel (MVVM), Layered Architecture, Microservices, Event-Driven Architecture.

Algorithmic Patterns:

These patterns focus on algorithms and computational processes. They provide templates for solving specific algorithmic problems.

Examples: Divide and Conquer, Greedy Algorithms, Dynamic Programming.

Idioms:

Idioms are not necessarily design patterns but are common practices or coding patterns that are specific to a particular language or technology.

Examples: RAII (Resource Acquisition Is Initialization) in C++, JavaBeans conventions in Java.

Anti-Patterns:

Anti-patterns are common solutions to recurring problems that turn out to be more counterproductive than beneficial. They represent practices to be avoided.

Examples: God Object, Spaghetti Code, Singleton Abuse.

